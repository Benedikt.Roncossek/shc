#!/bin/bash

# Define directories and files
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HASHFILE_DIR=$SCRIPT_DIR/hashfiles
CHANGELOG_DIR=$SCRIPT_DIR/changelogs
SHC_CONF=$SCRIPT_DIR/shc.conf
SHC_IGNORE=$SCRIPT_DIR/path_ignore.txt
SHC_SH=$SCRIPT_DIR/shc.sh
FIRSTRUNHASHFILE=$HASHFILE_DIR/"hashvalues_$(date +"%Y%m%d_%H:%M:%S")"


# Define default arguments.
MISSING_FILE=false


# Start Program
clear
echo "Initialise SHC Setup"
echo "#######################"
echo ""

# Set up folder structure
[ ! -d "$HASHFILE_DIR" ] && mkdir $HASHFILE_DIR
[ ! -d "$CHANGELOG_DIR" ] && mkdir $CHANGELOG_DIR
[ ! -f "$SHC_CONF" ] && echo "$SHC_CONF not found!" && MISSING_FILE=true
[ ! -f "$SHC_SH" ] && echo "$SHC_SH not found!" && MISSING_FILE=true
[ ! -f "$SHC_IGNORE" ] && touch SHC_IGNORE
[ ! -f "$FIRSTRUNHASHFILE" ] && touch $FIRSTRUNHASHFILE

# Abort script if main files are missing
# TODO: automatically download the project again
if $MISSING_FILE;then
  echo "Some files are missing!"
  exit
fi

# Make script executable
chmod +x $SHC_SH

# Download dependencies
yum install rhash mailx vixie-cron

# Setup cronjobs
service crond start
chkconfig crond on
# set a cronjob for 4:00 a.m.
(crontab -l ; echo "0 4 * * * cd $SCRIPT_DIR && $SHC_SH") | sort - | uniq - | crontab -
# crontab syntax
# * * * * * command

# 1 2 3 4 5 Befehl
# 1 = Minute (0-59)
# 2 = Stunde (0-23)
# 3 = Tag (0-31)
# 4 = Monat (1-12)
# 5 = Wochentag (0-7, Sonntag ist 0 oder 7)
# Befehl = Der auszuführende Befehl.

echo ""
echo "Cronjobs:"
crontab -l


# First time setup
# Run shc twice in order to obtain a changelog containing frequently changing files that are still hashed

# Check, if dir is empty, as an indicator, whether the ignore-file needs to be configured
if [ -z "$(ls -A $CHANGELOG_DIR)" ]; then
	echo "Initialise first time setup..."
	./shc.sh
	sleep 10
	./shc.sh
fi




