#!/bin/bash

# Define directories and files
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HASHFILE_DIR=$SCRIPT_DIR/hashfiles
CHANGELOG_DIR=$SCRIPT_DIR/changelogs
SHC_CONF=$SCRIPT_DIR/shc.conf
SHC_IGNORE=$SCRIPT_DIR/path_ignore.txt
SHC_SH=$SCRIPT_DIR/shc.sh

CURRENT_HASHFILE="hashvalues_$(date +"%Y%m%d_%H:%M:%S")"
CURRENT_CHANGELOG="changelog_$(date +"%Y%m%d_%H:%M:%S")"


# Parameters loaded from files
PATHS_TO_HASH=( )
PATHS_TO_IGNORE=( )
PATH_IGNORE=""
SEND_EMAIL_HASHVALUES=""
SEND_EMAIL_CHANGELOG=""

# Parameters for testing
TESTING="true"

########################################################################################################
### Read .conf file                                                                                  ###
########################################################################################################

function read_config {
	while read -r line; do
		[[ "$line" =~ ^#.*$ ]] && continue	# Skip lines starting with #
		[[ -z "$line" ]] && continue		# Skip blank lines
		array=( ${line} )
		#echo "${array[0]}"

		if [ "${array[0]}" = "PATHS_TO_HASH" ]; then
			PATHS_TO_HASH+=("${array[@]:2}")		# Remove the first two Elements: "KEYWORD" ":"
		fi

		if [ "${array[0]}" = "PATH_IGNORE" ]; then
			PATH_IGNORE=("${array[@]:2}")
			read_path_ignore
		fi

		if [ "${array[0]}" = "SEND_EMAIL_HASHVALUES" ]; then
			SEND_EMAIL_HASHVALUES=("${array[@]:2}")
		fi

		if [ "${array[0]}" = "SEND_EMAIL_CHANGELOG" ]; then
			SEND_EMAIL_CHANGELOG=("${array[@]:2}")
		fi



	done < "$SHC_CONF"

	# delete temporary variables
	array=""

	# For testing
	if [[ $TESTING == "true" ]]; then
		echo "PATHS_TO_HASH: ${PATHS_TO_HASH[@]}"
		echo "PATHS_TO_IGNORE: ${PATHS_TO_IGNORE[@]}"
		echo "PATH_IGNORE: $PATH_IGNORE"
		echo "SEND_EMAIL_HASHVALUES: $SEND_EMAIL_HASHVALUES"
		echo "SEND_EMAIL_CHANGELOG: $SEND_EMAIL_CHANGELOG"
	fi

}


########################################################################################################
### Read path_ignore.txt file                                                                                  ###
########################################################################################################


function read_path_ignore {
	#echo "start read_path_ignore"
	while read -r line; do
		[[ "$line" =~ ^#.*$ ]] && continue	# Skip lines starting with #
		[[ -z "$line" ]] && continue		# Skip blank lines
		PATHS_TO_IGNORE+=( ${line} )
		#echo "${line}"


	done < "$SHC_IGNORE"
}



########################################################################################################
### Create hashes and save the output to a file                                                      ###
########################################################################################################


function run_hashes {
	file="$HASHFILE_DIR/$CURRENT_HASHFILE"

	touch $file

	for i in "${PATHS_TO_HASH[@]}"
	do
		:
		echo "Start hashing $i"

		time nice -n 10 rhash --sha1 -p '%h,%p\n' -r $i > "$i".txt
		wc -l "$i".txt
		echo ""
		echo ""
		cat "$i".txt >> $file
		rm -f "$i".txt
	done

	echo ""
	echo "Hashvalues stored in: "
	echo "$file"
	echo ""

	# delete temporary variables
	file=""

}


########################################################################################################
### Take the last two hashfiles and check, if the output has changed using diff                      ###
########################################################################################################

function compare_hashes {
	file="$CHANGELOG_DIR/$CURRENT_CHANGELOG"

	# Grap the two latest hashfiles and execute a diff to compare their content
	temp="$(ls -t "$HASHFILE_DIR"/hashvalues_*)"
	array=($temp)
	file1=${array[0]}
	file2=${array[1]}

	# For testing
	if [[ $TESTING == "true" ]]; then
		echo ""
		echo "Those files will be compared"
		echo "$file1 vs $file2"
		echo ""
	fi

	# remove paths from file1 and file2, that are specified in path_ignore.txt
	if [ $PATH_IGNORE == "true" ]; then

		echo ""
		echo "================================================"
		echo "Removing Lines from Hashfile: $file1"
		echo "================================================"
		echo ""

		cat $file1 > difffile1.txt

		# Loop through every line in path_ignore.txt
    for i in "${PATHS_TO_IGNORE[@]}"
    do
    	:

			echo "" > temp1.txt

			# read every line of the hashfile and delete lines containing the substring from path_ignore.txt
	    while read -r line; do

				if [[ "${line}" != *"$i"* ]]; then
					echo "${line}" >> temp1.txt
				#else
				#	echo "removing line: $line"
				fi

      done < difffile1.txt
		cat temp1.txt > difffile1.txt
		done

		echo ""
  	echo "================================================"
    echo "Removing Lines from Hashfile: $file2"
    echo "================================================"
		echo ""

		cat $file2 > difffile2.txt

    for i in "${PATHS_TO_IGNORE[@]}"
    do
    	:

			echo "" > temp2.txt
	   	while read -r line; do

				if [[ "${line}" != *"$i"* ]]; then
					echo "${line}" >> temp2.txt
				#else
				#	echo "removing line: $line"
				fi

    	done < difffile2.txt
		cat temp2.txt > difffile2.txt
		done

		# Remove temporary files
		rm -f temp1.txt
		rm -f temp2.txt
	fi

	# Log the output from diff to a file
	diff_output="$(diff difffile1.txt difffile2.txt)"

	if [ -z "$diff_output" ]
	then
		echo "No files have been modified"
	else
		echo "Change detected. "
		echo "Writing to logfile $file"
		# Write changes to logfile
		echo "                        $(date +"%Y%m%d_%H:%M:%S")" >> "$file"
		diff difffile1.txt difffile2.txt >> "$file"

		#send_email_diff
		[[ $SEND_EMAIL_CHANGELOG == "true" ]] && { send_email_diff "$diff_output" "$file1" "$file2"; }

	fi

	# Removing temporary files
	rm -f difffile1.txt
	rm -f difffile2.txt

	# delete temporary variables
	file=""
	temp=""
	array=""
	file1=""
	file2=""

}

########################################################################################################
### Send Email with Changelog                                                                        ###
########################################################################################################

# Function Parameters: "$diff_output" "$file1" "$file2"
function send_email_diff {
	# Notify via Email
	echo ""
	echo "================================================"
	echo "An email with the Changelog will be sent to ukshiwi@gmail.com"
	echo "================================================"
	echo ""
	msg="A file has been modified!"$'\n'$'\n'
	msg="$msg""The automatic file checking program checked $2 and $3 and has detected a change in hash values since last checked."$'\n'
	msg="$msg""Please check $(hostname):$(pwd) and confirm, that the change was intentional!"$'\n'$'\n'
	msg="$msg""                        $(date +"%Y%m%d_%H:%M:%S")"$'\n'
	msg="$msg""$1"
	echo "$msg" | mailx -v -s "A file on $(hostname) was modified!" ukshiwi@gmail.com
	echo ""
}

########################################################################################################


function send_email_hashvalues {
	# Notify via Email
	echo ""
	echo "=============================================================="
	echo "An email with all hashvalues will be sent to ukshiwi@gmail.com"
	echo "=============================================================="
	echo ""
	msg="Content of Hashfile: $CURRENT_HASHFILE"$'\n'$'\n'
	msg="$msg""$(cat "$HASHFILE_DIR/$CURRENT_HASHFILE")"
	echo "$msg" | mailx -v -s "Hashfile $CURRENT_HASHFILE" ukshiwi@gmail.com
	echo ""
}


########################################################################################################
### Main                                                                                             ###
########################################################################################################

clear

echo "--- Starting System Hash Check ---"
echo "=================================="
echo ""
echo ""

read_config
run_hashes
compare_hashes

#send_email_hashvalues
[[ $SEND_EMAIL_HASHVALUES == "true" ]] && { send_email_hashvalues; }
