############################################
### Title:		SHC README
### Author:		Benedikt Roncossek       
### Created:	14.11.2019               
### Updated:	20.12.2019               
############################################

# Table of content

1. Introduction
2. Installation  
 - Configure email settings manually
 - Configure crontab manually
3. Links

# 1. Introduction

This folder contains the files and instructions to deploy a selfwritten,
lightweighted alternative to official Detection Intrusion Systems (IDS).  
The main file/executable is `shc.sh.`

The project can be downloaded from GitLab:
https://gitlab.com/Benedikt.Roncossek/shc.git

# 2. Installation

Use the install.sh script, to automatically setup a dayly hash test.

Ensure, that git is installed on the server.
```
sudo yum install git
```

Download the project from git.
```
git clone https://gitlab.com/Benedikt.Roncossek/shc.git
```

Make `install.sh` executable and run it.
```
chmod +x install.sh
sudo ./install.sh
```

## 2.1 Configure email settings manually

How to Configure mailx in linux 7 to send emails from terminal using GMAIL as an external SMTP Server. [1]
```
yum install -y mailx
```
Edit
```
vim /etc/mail.rc
```
Add the below lines and change "youremailadderess", "YOURPASSWORD" to your credentials.
```
set smtp=smtps://smtp.gmail.com:465
set smtp-auth=login
set smtp-auth-user=youremailadderess
set smtp-auth-password=YOURPASSWORD
set ssl-verify=ignore
set nss-config-dir=/etc/pki/nssdb/
Update the gmail account credentails over here and save.
```

Now we are all done , we will be using gmail to send email from our linux box.
We can test the Installation by sending a testmail via console.
```
echo "Test Email" | mail -v -s "Send an email via mailx" xxxxxxxx@gmail.com
```

## 2.2 Configure crontab manually

Crontab is a program, that periodically executes a task.  
In our case it executes our script.

To create a cronjob (for the current user!)
```
crontab -e
```
The syntax for a cronjob is

1 2 3 4 5 "The executable command"
```
- 1: Minute (0-59)
- 2: Hour (0-23)
- 3: Day (0-31)
- 4: Month (1-12)
- 5: Weekday (0-7, sunday is 0 or 7)
```

For further information check the Tutorial. [2]


# 3. Links
- [1] How to Configure mailx in linux 7 to send email|| mailx commands||wiz maverick
  https://www.youtube.com/watch?v=Cqueqz3W0VU
- [2] Crontab Syntax und Tutorial: Cronjobs unter Linux einrichten und verstehen
  https://www.stetic.com/developer/cronjob-linux-tutorial-und-crontab-syntax/
